# French translation for ldh-gui-suite
# Copyright (C) 2019
# This file is distributed under the same license as the ldh-gui-suite package.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: ldh-gui-suite\n"
"Report-Msgid-Bugs-To: ldh-gui-suite@packages.debian.org\n"
"POT-Creation-Date: 2020-09-09 13:28+0200\n"
"PO-Revision-Date: 2019-10-09 07:05+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' which is
#. a specific concept defined at <https://source.puri.sm/liberty/services>
#: ../templates:1001
msgid ""
"The \"Liberty Deckplan Host\" (LDH) is a single domain implementing the "
"concrete configuration plan defined at <https://source.puri.sm/liberty/"
"services>."
msgstr ""
"Le « Liberty Deckplan Host » (LDH) est un domaine unique implémentant le "
"plan de configuration concret défini sur <https://source.puri.sm/liberty/"
"services>."

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"This name will also be used by other programs. It should be the single, "
"fully qualified domain name (FQDN)."
msgstr ""
"Ce nom sera utilisé aussi par d'autres programmes. Il devrait être un nom de "
"domaine unique et pleinement qualifié (FQDN)."

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:1001 ../templates:2001 ../templates:3001 ../templates:4001
#: ../templates:5001
msgid ""
"Leave blank to use a default value (currently \"${defaultvalue}\"), and to "
"permit eventual automatic change of that value without asking."
msgstr ""
"Veuillez laisser vide pour utiliser une valeur par défaut (actuellement, "
"« ${defaultvalue} » et pour permettre le changement éventuel automatique de "
"cette valeur sans qu'une question ne soit posée."

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or 'Hub'
#. which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:2001
msgid "Descriptive name for Liberty Deckplan Host service Hub:"
msgstr "Nom décrivant le service Hub de Liberty Deckplan Host :"

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or 'Hub'
#. which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:2001
msgid "\"Hub\" is a service to manage your Liberty Deckplan Host account."
msgstr "« Hub » est un service pour gérer votre compte Liberty Deckplan Host."

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"This descriptive name will also be used by other programs. It should be a "
"short string usable within a longer description sentence."
msgstr ""
"Ce nom descriptif sera utilisé aussi par d'autres programmes. Ce devrait "
"être une chaîne courte utilisable à l'intérieur d'une phrase de description "
"plus longue."

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or 'Hub'
#. which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:3001
msgid "URI for Liberty Deckplan Host service Hub:"
msgstr "URI du service Hub de Liberty Deckplan Host :"

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or 'Hub'
#. which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:3001
msgid ""
"\"Hub\" is a service to manage your Liberty Deckplan Host account, online "
"accessible at this URI."
msgstr ""
"« Hub » est un service pour gérer votre compte Liberty Deckplan Host, "
"accessible en ligne à cette adresse."

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:3001 ../templates:4001 ../templates:5001
#, fuzzy
#| msgid ""
#| "This URI will also be used by other programs. It should be the single "
#| "unified reference information (URI)."
msgid ""
"This URI will also be used by other programs. It should be the single "
"Uniform Resource Identifier (URI)."
msgstr ""
"Cet URI sera utilisé aussi par d'autres programmes. Ce devrait être une "
"information unifiée de référence unique (URI)."

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or
#. 'Chat' which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:4001
#, fuzzy
msgid "URI for Liberty Deckplan Host service Chat:"
msgstr "URI du service Chat de Liberty Deckplan Host :"

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or
#. 'Chat' which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:4001
#, fuzzy
msgid "\"Chat\" is a microblogging service part of Liberty Deckplan Host."
msgstr ""
"« Chat » est un service de microblogage qui fait partie de Liberty "
"Deckplan Host."

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or
#. 'Social' which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:5001
msgid "URI for Liberty Deckplan Host service Social:"
msgstr "URI du service Social de Liberty Deckplan Host :"

#. Type: string
#. Description
#. Translators, please do NOT translate 'Liberty Deckplan Host' or
#. 'Social' which are specific concepts defined at
#. <https://source.puri.sm/liberty/services>
#: ../templates:5001
msgid "\"Social\" is a microblogging service part of Liberty Deckplan Host."
msgstr ""
"« Social » est un service de microblogage qui fait partie de Liberty "
"Deckplan Host."
