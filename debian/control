Source: ldh-gui-suite
Section: utils
Priority: optional
Maintainer: Jonas Smedegaard <dr@jones.dk>
Build-Depends:
 autoconf-archive,
 debhelper-compat (= 13),
 debhelper,
 cmark-gfm <!nodoc>,
 librsvg2-bin,
 po-debconf,
 scour,
Standards-Version: 4.6.2
Homepage: https://source.puri.sm/liberty/ldh_gui_suite
Vcs-Git: https://salsa.debian.org/liberty-one-team/ldh-gui-suite.git
Vcs-Browser: https://salsa.debian.org/liberty-one-team/ldh-gui-suite
Rules-Requires-Root: no

Package: ldh-gui-suite
Architecture: all
Depends:
 epiphany-browser,
 ${misc:Depends},
Description: graphical clients for Liberty Deckplan Host services
 LDH GUI Suite provides access to Liberty Deckplan Host (LDH) services
 using GNOME Web for application wrappers.
 .
 The suite provides application wrappers for the following clients:
  * Chat - client for the instant messaging service Element
  * Social - client for the social networking service Smilodon
  * Hub - configuration interface for the Liberty Deckplan Host
 .
 Element (formerly Riot and Vector) is a web-based instant messaging client
 <https://element.io/>
 implementing the Matrix protocol.
 .
 Smilodon is a self-hosted and federated social networking service
 <https://source.puri.sm/liberty/smilodon>,
 similar to (and created as a fork of) Mastodon.
 .
 A Liberty Deckplan Host (LDH) is a single domain
 implementing the concrete configuration plan
 defined at <https://source.puri.sm/liberty/services>.
