# LDH GUI Suite

[project] | [code] | [tracker] | [debian]

A Liberty Deckplan Host (LDH) is a single domain
implementing the concrete configuration plan defined
at <https://source.puri.sm/liberty/services>

This package contains resources for LDH clients. Including:

 *  icons for all services and wrappers for web interfaces
 *  default configuration for the flagship host `librem.one`
 *  reconfiguration for any other host


## Install

If the GUI Suite is not pre-installed on your system,
the preferred installation method is your package manager.
Installing the suite package will install
all recommended clients and their dependencies on your system.

Runtime requirements:

 *  GNOME Web (a.k.a. Epiphany) 3.19.1 or newer


### Debian

On Debian and derived systems (including Ubuntu and PureOS):

```
sudo apt install ldh-gui-suite
```


### From source tarball

Build requirements when installing from source:

 *  make
 *  scour
 *  rsvg-convert
 *  gtk-update-icon-cache

Installing from source tarball:

```
./configure
make
sudo make install
```


### From Git snapshot

Additional build requirements when installing from Git snapshot:

 *  autoconf 2.69 or newer
 *  autoconf-archive 2015-02-05 or newer
 *  automake

Installing from Git snapshot:

```
./bootstrap
./configure
make
sudo make install
```


## Configuration

Consult your distribution how to configure when installing a package.

When installing from source, you can customize
which Liberty Deckplan Host to use,
by passing the `--enable-domain=DOMAIN` option to configure.

Alternatively you can freely define the full URIs to use,
by passing either of the options `--enable-hub-uri=URI`,
`--enable-chat-uri=URI`, or `--enable-social-uri=URI` to configure.


## Usage


### Hub

* Click on the Hub icon to launch.
* Enter your credentials.
* Manage your account.


### Chat

* Click on the Chat icon to launch.
* Enter your credentials.
* Start chatting!


### Social

* Click on the Social icon to launch.
* Enter your credentials.
* Start posting!


## Implementation

Web wrappers are currently implemented as GNOME Web applications,
configured by debconf. See
[How do I define a system-wide web application?](http://wiki.gnome.org/Apps/Web/Docs/FrequentlyAskedQuestions#How_do_I_define_a_system-wide_web_application.3F)
(GNOME Web FAQ) for details.

This implementation should work on any Debian-based distribution that
includes GNOME Web.


## Host compatibility

Clients should work on any host that follows the Liberty Deckplan.
For example, any social server with the domain `social.example.com`
and which supports `/web/getting-started` as a login path. The latter
includes any Smilodon, Mastodon or Florence instance.


## Sharing and contributions

LDH GUI Suite  
<https://source.puri.sm/liberty/ldh_gui_suite>  
Copyright 2019-2020, Jonas Smedegaard <dr@jones.dk>  
Copyright 2019-2020, Purism, SPC  
SPDX-License-Identifier: AGPL-3.0-or-later  

Shared under AGPL-3.0-or-later. We adhere to the Community Covenant
1.0 without modification, and certify origin per DCO 1.1 with a
signed-off-by line. Contributions under the same terms are
welcome.

For details see:

 *  [COPYING.AGPL.md], full license text
 *  [CODE_OF_CONDUCT.md], full conduct text
 *  [CONTRIBUTING.DCO.md], full origin text (`git -s`)

<!-- * [CONTRIBUTING.md], additional contribution notes -->

<!-- Links -->

[project]: https://source.puri.sm/liberty/ldh_gui_suite
[code]: https://source.puri.sm/liberty/ldh_gui_suite/tree/master
[tracker]: https://source.puri.sm/liberty/ldh_gui_suite/issues
[wiki]: https://source.puri.sm/liberty/ldh_gui_suite/wikis/home
[debian]: https://tracker.debian.org/pkg/ldh-gui-suite
[SETUP.md]: SETUP.md
[COPYING.AGPL.md]: COPYING.AGPL.md
[CODE_OF_CONDUCT.md]: CODE_OF_CONDUCT.md
[CONTRIBUTING.DCO.md]: CONTRIBUTING.DCO.md
[COPYING.md]: COPYING.md
[CONTRIBUTING.md]: CONTRIBUTING.md
